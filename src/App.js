import React from 'react'

import Todo from './Todo'
import Navbar from './layout/Navbar'

import './App.css'

function App() {
  return (
    <React.Fragment>
      <Navbar />
      <div className="App">
        <h1>Cosas por hacer</h1>
        <Todo />
      </div>
    </React.Fragment>
  )
}

export default App
