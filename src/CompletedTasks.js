import React from 'react'

import './CompletedTasks.css'

class CompletedTasks extends React.Component {
  render() {
    const { completed } = this.props
    return (
      <details>
        <summary>Tareas completadas <span>({completed.length})</span></summary>
        <ul>
          {
            completed.map((value, index) => {
              return (
                <li key={index}>
                  <span>{value.title}</span>
                </li>
              )
            })
          }
        </ul>
      </details>
    )
  }
}

export default CompletedTasks
