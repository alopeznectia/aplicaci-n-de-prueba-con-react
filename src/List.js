import React from 'react'

import './List.css'

class TodoList extends React.Component {
  constructor(props) {
    super(props)
    this.removeItem = this.removeItem.bind(this)
    this.changeStatus = this.changeStatus.bind(this)
  }

  removeItem(index) {
    (index || index === 0) && this.props.remove(index)
  }

  changeStatus(index) {
    (index || index === 0) && this.props.markAsDone(index)
  }

  render() {
    const { items } = this.props
    return (
      <ul className="pending-list">
        {
          items.map((value, index) => {
            return (
              <li key={index}
                className={value.completed ? 'done' : ''}>
                <input type="checkbox"
                  checked={value.completed}
                  onChange={() => this.changeStatus(index)} />
                <span>{value.title}</span>
                <button type="button"
                  className="btn-remove"
                  onClick={() => this.removeItem(index)}>x</button>
              </li>
            )
          })
        }
      </ul >
    )
  }
}

export default TodoList
