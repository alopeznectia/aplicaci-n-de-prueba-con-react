import React from 'react'

import TodoList from './List'
import CompletedTasks from './CompletedTasks'

import './Todo.css'

class Todo extends React.Component {
  constructor(props) {
    super(props)

    this.onSubmit = this.onSubmit.bind(this)
    this.addItem = this.addItem.bind(this)
    this.removeItem = this.removeItem.bind(this)
    this.updateNewItem = this.updateNewItem.bind(this)
    this.saveAsCompleted = this.saveAsCompleted.bind(this)
    this.markAsDone = this.markAsDone.bind(this)
    this.fetchMoreTasks = this.fetchMoreTasks.bind(this)

    this.state = {
      newItem: '',
      items: [
        {
          title: 'Laboratorio de React',
          completed: false
        },
        {
          title: 'Estudiar',
          completed: false
        },
        {
          title: 'Ir a Mallplaza',
          completed: false
        }
      ],
      completed: []
    }
  }

  onSubmit(event) {
    const { newItem } = this.state

    event.preventDefault()

    this.addItem(newItem)
  }

  addItem(newItem) {
    const { items: currentItems } = this.state

    this.setState({ items: [...currentItems, { title: newItem }] })
    this.setState({ newItem: '' })
  }

  removeItem(index) {
    const { items } = this.state

    items.splice(index, 1)

    this.setState({ items })
  }

  saveAsCompleted(index) {
    const { items, completed: currentlyDone } = this.state

    const completedItem = items.splice(index, 1)[0]
    const completed = [...currentlyDone, completedItem]

    this.setState({ items, completed })
  }

  markAsDone(index) {
    const { items } = this.state
    const modifiedItem = items[index]

    modifiedItem.completed = !modifiedItem.completed

    items[index] = modifiedItem

    this.setState({ items })

    setTimeout(() => this.saveAsCompleted(index), 200)
  }

  updateNewItem(event) {
    this.setState({ newItem: event.target.value })
  }

  fetchMoreTasks() {
    const endpoint = 'https://jsonplaceholder.typicode.com/todos'

    fetch(endpoint)
      .then((response) => response.json())
      .then((tasks) => {
        if (Array.isArray(tasks))
          tasks
            .splice(0, 3)
            .forEach((task) => this.addItem(task.title))
      })
  }

  render() {
    const { newItem, items: todoItems, completed } = this.state
    return (
      <React.Fragment>
        <section data-todo>
          <TodoList items={todoItems}
            remove={this.removeItem}
            markAsDone={this.markAsDone} />

          <form onSubmit={this.onSubmit}>
            <input type="text"
              placeholder="Agrega un elemento..."
              value={newItem}
              onChange={this.updateNewItem} />
            <button type="submit"
              disabled={!newItem}>Agregar</button>
          </form>
          <div data-fetch-more>
            <p>¿Libreta?</p>
            <button type="button"
              onClick={this.fetchMoreTasks}>Obtén nuevas tareas</button>
          </div>
        </section>

        <section data-done>
          <CompletedTasks completed={completed} />
        </section>
      </React.Fragment >
    )
  }
}

export default Todo
