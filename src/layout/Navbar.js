import React from 'react'

import Sidebar from './Sidebar'

import './Navbar.css'

class Navbar extends React.Component {
  constructor(props) {
    super(props)
    this.toggleSidebar = this.toggleSidebar.bind(this)

    this.state = { activeSidebar: false }
  }

  toggleSidebar() {
    const { activeSidebar: currentState } = this.state

    this.setState({ activeSidebar: !currentState })
  }

  render() {
    const { activeSidebar } = this.state
    return (
      <React.Fragment>
        <section data-navbar>
          <div>
            <span role="img"
              aria-label="navbar-icon"
              onClick={this.toggleSidebar}>📶</span>
          </div>
          <div>Laboratorio</div>
          <div>
            <span role="img" aria-label="navbar-icon">🔍</span>
          </div>
        </section>
        <Sidebar open={activeSidebar}
          close={this.toggleSidebar} />
      </React.Fragment>
    )
  }
}

export default Navbar
