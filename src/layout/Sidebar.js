import React from 'react'

import './Sidebar.css'

class Sidebar extends React.Component {
  constructor(props) {
    super(props)
    this.toggle = this.toggle.bind(this)
  }

  toggle() {
    this.props.close()
  }

  render() {
    const { open } = this.props

    return (
      <section data-sidebar
        className={open ? 'open' : 'closed'}>
        <h2>⚛ Laboratorio de React</h2>
        <hr />  
        <h4>Contenido</h4>
        <ul>
          <li>Componentes básicos</li>
          <li>Uso de props y state</li>
          <li>Comunicación entre componentes</li>
          <li>HTTP Requests</li>
          <li>Lifecycles</li>
        </ul>
        <h4>Pendientes</h4>
        <ul>
          <li>Rutas</li>
          <li>Componentes lazy</li>
          <li>Redux</li>
        </ul>
        <button type="button"
          onClick={this.toggle}>Cerrar</button>
      </section>
    )
  }

  componentDidUpdate() {
    const listenForOutsideClicks = (event) => {
      const { target } = event
      const clickedOutsideSidebar = !target.matches('section[data-sidebar] *')

      if (clickedOutsideSidebar)
        this.toggle()
    }

    document.addEventListener('click', listenForOutsideClicks)
  }
}

export default Sidebar
